import { Component } from '@angular/core';
import { ViewController, IonicPage } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  public splashConfig: Object;

  constructor(public viewCtrl: ViewController, public splashScreen: SplashScreen) {

    this.splashConfig = {
      loop: false,
      prerender: true,
      autoplay: true,
      autoloadSegments: true,
      path: 'file:///home/atec3/Imagens/loading.gif'
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SplashPage');
  }

  ionViewDidEnter() {
 
    // hide default splash screen
    this.splashScreen.hide();



    // hide our animated splash page
    setTimeout(() => {
      this.viewCtrl.dismiss();
    }, 3000);
 
  }

}
